package dataglen.sup;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    MediaPlayer testsound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.audio);
        testsound = MediaPlayer.create(MainActivity.this, R.raw.bv);
        testsound.setAudioStreamType(AudioManager.STREAM_MUSIC);
        testsound.start();

        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.STREAM_MUSIC);
        audioManager.setSpeakerphoneOn(false);

        ImageButton mute = (ImageButton) findViewById(R.id.imaged);
        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

          //     audioManager.setMicrophoneMute(false);
                AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                audioManager.setMode(AudioManager.STREAM_MUSIC);
               audioManager.setSpeakerphoneOn(false);
         //       audioManager.setMicrophoneMute(true);

            }
        });

        ImageButton speaker = (ImageButton) findViewById(R.id.speaker);
        speaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                audioManager.setMode(AudioManager.STREAM_MUSIC);
                audioManager.setSpeakerphoneOn(true);
            }
        });


    }
}
